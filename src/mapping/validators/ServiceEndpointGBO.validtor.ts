import Joi                              from "joi";
import logger                           from "../../utils/Logger"
import {PostRespBusinessObjects}        from "../bussObj/Response";
import AppConfig                        from "../../config/AppConfig";

export default class ServiceEndpointSchemaModel{
    
    ServiceEndpointSchema = Joi.object({
        "service-endpoint": {
            "endpoint_id": Joi.string().min(8).max(56).required(),
            "srv_id": Joi.string().min(8).max(56).required(),
            "version_id": Joi.string().min(8).max(56).required(),
            "endpoint_uri": Joi.string().min(1).max(64).required(),
            "http_method": Joi.string().allow('POST', 'GET', 'PUT', 'DELETE')
        }
    });

    /**
     * Validates request against the defined structure
     * @param request (JSON) request to validate
     */
    public validateRequest(request:any){        
        let {error,value} = this.ServiceEndpointSchema.validate(request);
        if(error){
            logger.error(`Validation error ${error}`);            
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Schema validation error",trace);                        
            }else{
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Schema validation error");                        
            }
            
        }else{            
            return value;
        }
    }
}