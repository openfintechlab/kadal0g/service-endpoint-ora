/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Controller for managing all operations of the Service-Endpoint
 * 
 */

import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import OracleDBInteractionUtil          from "../utils/OracleDBInteractionUtil";
import util                             from "util";
import AppConfig                        from "../config/AppConfig";

export default class ServiceEndpointController{
    private readonly _MAX_RECORD_ABUSE:number = 100;    

    /**
     * Creates service-endpoint in the database
     * @param reqJSON (JSON) Json object for creating the service-endpoint
     */
    public async create(reqJSON:any){
        logger.info(`Creating service-endpoint entitiy in the data store`);
        logger.debug(`Request received for creating service-endpoint: ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})} `);        
        try{
            let response = await this.createServiceEndpoint(reqJSON);
            logger.info(`Service endpoint created sucessfully`);
            return response;
        }catch(error){
            logger.error(`Error while creating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

    /**
     * Updates service-endpoint
     * @param reqJSON (JSON) JSON object to udpate the service-endpoint
     */
    public async update(reqJSON: any){
        logger.info(`Updating service-endpoint entitiy in the data store`);
        logger.debug(`Request received for updating service-endpoint: ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})} `);        
        try{
            let response = await this.updateServiceEndpoint(reqJSON);
            logger.info(`Service endpoint updated sucessfully`);
            return response;
        }catch(error){
            logger.error(`Error while updating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

    /**
     * Deletes specific endpoint using endpoint_id
     * @param endpoint_id (number) Endpoint ID to delete
     */
    public async deleteByEndpointID(endpoint_id:string){
        logger.info(`Deleting service-endpoint entitiy from the data store: ${endpoint_id}`);
        try{
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT endpoint_id FROM med_service_endpoints WHERE endpoint_id=:v0`,[endpoint_id]);
            if(selResp.rows.length <= 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8403","Required key does not exist in the db");
            }
            let sql:string = `DELETE FROM med_service_endpoints WHERE endpoint_id = :v0`;
            let binDel = [endpoint_id];
            let delResp = await OracleDBInteractionUtil.executeUpdate(sql,binDel);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");            
        }catch(error){
            logger.error(`Error while updating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

   /**
     * Deletes all endpoints using service id
     * @param srv_id (number) Service ID
     */
    public async deleteByServiceID(srv_id:string){
        logger.info(`Deleting service-endpoint entitiy from the data store: ${srv_id}`);
        try{
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT endpoint_id FROM med_service_endpoints WHERE srv_id=:v0`,[srv_id]);
            if(selResp.rows.length <= 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8403","Required key does not exist in the db");
            }
            let sql:string = `DELETE FROM med_service_endpoints WHERE srv_id = :v0`;
            let binDel = [srv_id];
            let delResp = await OracleDBInteractionUtil.executeUpdate(sql,binDel);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");            
        }catch(error){
            logger.error(`Error while updating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

    /**
     * Get all service endpoints
     * @param from From row count
     * @param to to row count
     */
    public async getAll(from:number=1, to:number=10){
        logger.info(`Getting all service-endpoints record from the db starting from: ${from} - to - ${to}`);
        to = (to > this._MAX_RECORD_ABUSE)? this._MAX_RECORD_ABUSE: to;                    
        if(from > to){
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8355","Invalid input");
        }
        let sql:string = `select RES.*,(SELECT COUNT(*) FROM med_service_endpoints) as TTL_ROWS from (
            SELECT version_id,endpoint_id,srv_id,endpoint_uri,http_method,created_on,updated_on,created_by,updated_by,last_ops_id FROM  med_service_endpoints
            ) RES where ROWNUM BETWEEN :v1 and :v2 order by rownum `;
        let binds= [from,to];
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql, binds);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let bussObj = await this.createBusinessObjectForAll(result);
            return bussObj;
        }catch(error){
            logger.error(`Error while updating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

     /**
      * Get All endpoints against the service id
      * @param srv_id (string) Service ID to fetch the endpoints agains
      */
    public async get(srv_id:any){
        logger.info(`Getting all service-endpoint record from the db starting for service_id: ${srv_id}`);        
        let sql:string = `SELECT version_id,endpoint_id,srv_id,endpoint_uri,http_method,created_on,updated_on,created_by,updated_by,last_ops_id FROM  med_service_endpoints WHERE srv_id=:v0 ORDER BY updated_on desc`;
        let binds= [srv_id];
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql, binds);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let bussObj = await this.createBusinessObjectForAll(result);
            return bussObj;
        }catch(error){
            logger.error(`Error while fetching details from the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

         /**
      * Get All endpoints against the service id
      * @param srv_id (number) Service ID to fetch the endpoints agains
      */     
     public async getBySrvAndVer_ID(srv_id:string,ver_id:string){
        logger.info(`Getting all service-endpoint record from the db starting for service_id = ${srv_id}`);        
        let sql:string = `SELECT version_id,endpoint_id,srv_id,endpoint_uri,http_method,created_on,updated_on,created_by,updated_by,last_ops_id FROM  med_service_endpoints WHERE srv_id=:v0 AND version_id=:v1 ORDER BY updated_on desc`;
        let binds= [srv_id,ver_id];
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql, binds);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let bussObj = await this.createBusinessObjectForAll(result);
            return bussObj;
        }catch(error){
            logger.error(`Error while updating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Error While performing the Operation");            
            }
        }
    }

    private async createServiceEndpoint(reqJSON:any){
        let srvEndpointObj = reqJSON['service-endpoint'];
        // [FB:20201019]: Added new field version_id, after ammendment in the relationship service_master -> service_version -> service_endpoints        
        let sqlIns = `INSERT INTO med_service_endpoints (version_id,endpoint_id, srv_id, endpoint_uri, http_method, created_by, updated_by, last_ops_id) VALUES ( :v0, :v1, :v2, :v3, :v4, :v5, :v6, :v7)`;
        let bindIns = [srvEndpointObj.version_id,srvEndpointObj.endpoint_id,srvEndpointObj.srv_id,srvEndpointObj.endpoint_uri,srvEndpointObj.http_method,'SYSTEM','SYSTEM',null];
        try{            
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT srv_id FROM med_service_endpoints where srv_id=:v0 AND endpoint_uri=:v1 AND http_method=:v2`,[srvEndpointObj.srv_id, srvEndpointObj.endpoint_uri,srvEndpointObj.http_method] );
            if(selResp.rows.length > 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Service already exist");
            }
            let result = await OracleDBInteractionUtil.executeUpdate(sqlIns,bindIns);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`Error while creating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8401","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8401","Error While performing the Operation");
            }
        }
    }

    private async updateServiceEndpoint(reqJSON:any){
        let srvEndpointObj = reqJSON['service-endpoint'];
        let sqlIns = `UPDATE med_service_endpoints SET version_id = COALESCE(:v7, version_id), srv_id = COALESCE(:v1,srv_id),   endpoint_uri = COALESCE(:v2,endpoint_uri),   http_method = COALESCE(:v3,http_method),   updated_on = CURRENT_TIMESTAMP, updated_by = COALESCE(:v4,updated_by),   last_ops_id = COALESCE(:v5,last_ops_id) WHERE   endpoint_id = :v6`;
        let bindIns = [srvEndpointObj.srv_id,srvEndpointObj.endpoint_uri,srvEndpointObj.http_method,'SYSTEM',null,srvEndpointObj.endpoint_id, srvEndpointObj.version_id];
        try{            
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT srv_id FROM med_service_endpoints where srv_id=:v0 AND endpoint_uri=:v1 AND http_method=:v2 AND endpoint_id !=:v3`,[srvEndpointObj.srv_id, srvEndpointObj.endpoint_uri,srvEndpointObj.http_method,srvEndpointObj.endpoint_id] );
            if(selResp.rows.length > 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8402","Service already exist");
            }
            let result = await OracleDBInteractionUtil.executeUpdate(sqlIns,bindIns);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`Error while creating service endpoint: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8401","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8401","Error While performing the Operation");
            }
        }
    }

    private async createBusinessObjectForAll(dbResult:any){
        return new Promise<any>((resolve:any,reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8353","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    "trace": []
                },
                "service-endpoints": []
            }; 
            if(AppConfig.config.OFL_MED_NODE_ENV !== undefined // [FB:19122020] BUGFIX! Error in-case this value is not defined
                && AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'
                    && dbResult.rows[0].TTL_ROWS){
                bussObj.metadata['trace'].push(
                    {
                        "source": "TotalRows",
                        "description": dbResult.rows[0].TTL_ROWS
                    }
                );
            }                   
            for(let i:number =0; i< dbResult.rows.length; i++){
                bussObj["service-endpoints"].push({ 
                    "endpoint_id": dbResult.rows[i].ENDPOINT_ID,                    
                    "srv_id": dbResult.rows[i].SRV_ID,
                    "version_id": dbResult.rows[i].VERSION_ID,
                    "endpoint_uri": dbResult.rows[i].ENDPOINT_URI,
                    "http_method": dbResult.rows[i].HTTP_METHOD,                    
                    "created_on": dbResult.rows[i].CREATED_ON,
                    "updated_on": dbResult.rows[i].UPDATED_ON,
                    "created_by": dbResult.rows[i].CREATED_BY,
                    "updated_by": dbResult.rows[i].UPDATED_BY,
                    "last_ops_id": dbResult.rows[i].LAST_OPS_ID
                });
            }
            resolve(bussObj);
        });
    }
}