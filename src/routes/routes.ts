/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                          from "express";
import {AppHealth}                      from "../config/AppConfig";
import logger                           from "../utils/Logger";
import util                             from "util";
import {PostRespBusinessObjects}        from "../mapping/bussObj/Response";
import ServiceEndpointSchemaModel       from "../mapping/validators/ServiceEndpointGBO.validtor";
import ServiceEndpointController        from "../mapping/ServiceEndpointController";


const router: any = express.Router();


/**
 * Endpoint for doing HTTP-PUT / Insert in the database
 */
router.post('/',async (request:any,response:any) =>{
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for creating service-endpoint`);
    try{
        let parsedObj:any = await new ServiceEndpointSchemaModel().validateRequest(request.body);
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            // Call service controller            
            let respObj = await new ServiceEndpointController().create(parsedObj);
            response.status(201);
            response.send(respObj);            
        }        

    }catch(error){
        logger.debug(`Error occured while creating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

router.put('/', async(request:any,response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for updating service-endpoint`);
    try{
        let parsedObj:any = await new ServiceEndpointSchemaModel().validateRequest(request.body);
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            // Call service controller            
            let respObj = await new ServiceEndpointController().update(parsedObj);
            response.status(201);
            response.send(respObj);            
        }        
    }catch(error){
        logger.debug(`Error occured while updating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

/**
 * Delete a specific endpoint based on the endpoint_id OR service_id
 */
router.delete('/:id',async(request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for deleting service-endpoint`);
    try{
        let respObj;
        if(request.query.by_service_id){            
            respObj = await new ServiceEndpointController().deleteByServiceID(request.params.id); 
        }else{
            respObj = await new ServiceEndpointController().deleteByEndpointID(request.params.id); 
        }        
        response.status(201);
        response.send(respObj);
    }catch(error){
        logger.debug(`Error occured while deleting service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }    
});

/**
 * Get all 
 * uses query parameter: from and to
 */
router.get('/', async(request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for getting all service-endpoints`);
    let from:number, to:number;
    from = (request.query.from)? request.query.from : undefined;
    to = (request.query.to)? request.query.to : undefined;
    try{
        let parsedObj = await new ServiceEndpointController().getAll(from,to); 
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(404);
            response.send(parsedObj)
        }else{            
            response.status(200);
            response.send(parsedObj);            
        }                
    }catch(error){
        logger.debug(`Error occured while deleting service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){           
            if(error.metadata.status === '8353'){
                response.status(404);
            } 
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

router.get(`/:srv_id`, async(request:any,response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");   
    logger.info(`Procedure called for getting service-endpoint based on the service_Id`); 
    try{
        let parsedObj = await new ServiceEndpointController().get(request.params.srv_id); 
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(404);
            response.send(parsedObj)
        }else{
            // Call service controller                        
            response.status(200);
            response.send(parsedObj);            
        }               
    }catch(error){
        logger.debug(`Error occured while getting service version based on srv_id:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)        
        response.status(500);
        if(error.metadata){            
            if(error.metadata.status === '8353'){
                response.status(404);
            }
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

router.get(`/:srv_id/:ver_id`, async(request:any,response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");   
    logger.info(`Procedure called for getting service-endpoint based on the service_Id and version_id`); 
    try{
        let parsedObj = await new ServiceEndpointController().getBySrvAndVer_ID(request.params.srv_id, request.params.ver_id); 
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(404);
            response.send(parsedObj)
        }else{
            // Call service controller                        
            response.status(200);
            response.send(parsedObj);            
        }      
    }catch(error){
        logger.debug(`Error occured while deleting service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){          
            if(error.metadata.status === '8353'){
                response.status(404);
            }  
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});


export default router;